----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/14/2018 09:30:47 AM
-- Design Name: 
-- Module Name: IFSim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IFSim is
--  Port ( );
end IFSim;

architecture Behavioral of IFSim is
signal WE:             std_logic:='0';
signal reset:          std_logic:='0';
signal clk:            std_logic:='0';
signal BranchAddress:  std_logic_vector(31 downto 0):=X"0000_0000";
signal JumpAddress:    std_logic_vector(31 downto 0):=X"0000_0000";
signal JCS:            std_logic:='0';
signal PCSrc:          std_logic:='0';
signal Instruction:    std_logic_vector(31 downto 0);
signal PC:             std_logic_vector(31 downto 0):=X"0000_0000";
       
-- Declararea unei constante cu perioada semnalului de ceas
constant CLK_PERIOD : TIME := 10 ns;
    
begin
gen_clk: process
begin
    Clk <= '0';
    wait for (CLK_PERIOD/2);
    Clk <= '1';
    wait for (CLK_PERIOD/2);
end process gen_clk;

--------Instantiere componenta IFetch--------------
DUT: entity work.IFetch port map(
            WE           => WE,
			reset        => reset,
			clk          => clk,
			BranchAddress=> BranchAddress,
			JumpAddress  => JumpAddress,
			JCS          => JCS,
			PCSrc        => PCSrc,
			Instruction  => Instruction,
			PC           => PC);

gen_vect_test: process
     
    variable NrErori : Integer := 0;
    begin

    reset <= '1';
    wait for CLK_PERIOD;
    reset <= '0';
    
    if PC /= X"0000_0001" then
        report "Eroare: PC este: " & INTEGER'image(conv_integer(PC)) & " la t = " & TIME'image(now) severity ERROR;
        NrErori := NrErori + 1;
    end if;
    
    --wait for CLK_PERIOD;
    WE <= '1';
    --1
    if Instruction /= B"000000_00001_00010_00011_00000_100000" then
            report "Eroare: Instruction este: " & INTEGER'image(conv_integer(Instruction)) & " la t = " & TIME'image(now)
                    & " valoare asteptata: " & INTEGER'image(conv_integer(B"000000_00001_00010_00011_00000_100000")) severity ERROR;
            NrErori := NrErori + 1;
        end if;
        wait for CLK_PERIOD;
    --2    
    if Instruction /= B"000000_00010_00001_00011_00000_100010" then
            report "Eroare: Instruction este: " & INTEGER'image(conv_integer(Instruction)) & " la t = " & TIME'image(now)
                    & " valoare asteptata: " & INTEGER'image(conv_integer(B"000000_00010_00001_00011_00000_100010")) severity ERROR;
            NrErori := NrErori + 1;
        end if;    
        wait for CLK_PERIOD;
    --3        
    if Instruction /= B"000000_00001_00010_00011_00000_100100" then
        report "Eroare: Instruction este: " & INTEGER'image(conv_integer(Instruction)) & " la t = " & TIME'image(now) 
                & " valoare asteptata: " & INTEGER'image(conv_integer(B"000000_00001_00010_00011_00000_100100")) severity ERROR;
        NrErori := NrErori + 1;
    end if;
    wait for CLK_PERIOD;
    --4            
    if Instruction /= B"000000_00001_00010_00011_00000_100101" then
        report "Eroare: Instruction este: " & INTEGER'image(conv_integer(Instruction)) & " la t = " & TIME'image(now) 
                & " valoare asteptata: " & INTEGER'image(conv_integer(B"000000_00001_00010_00011_00000_100101")) severity ERROR;
        NrErori := NrErori + 1;
    end if; 
    wait for CLK_PERIOD;
    --5                
    if Instruction /= B"000000_00001_00010_00011_00000_100110" then
        report "Eroare: Instruction este: " & INTEGER'image(conv_integer(Instruction)) & " la t = " & TIME'image(now) 
                & " valoare asteptata: " & INTEGER'image(conv_integer(B"000000_00001_00010_00011_00000_100110")) severity ERROR;
        NrErori := NrErori + 1;
    end if;
    wait for CLK_PERIOD;
    
    report "Numarul de erori: " & INTEGER'image(NrErori);
    wait;
end process;

end Behavioral;
