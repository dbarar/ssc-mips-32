----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    05:16:11 04/02/2015 
-- Design Name: 
-- Module Name:    IDecode - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity IDecode is
	Port ( 
	        clk:         in std_logic;
			Instruction: in std_logic_vector(31 downto 0);
			WriteData:   in std_logic_vector(31 downto 0);
			RegWrite:    in std_logic;
			RegWrite2:   in std_logic;
			RegDst:      in std_logic;
			ExtOp:       in std_logic;
			ReadData1:   out std_logic_vector(31 downto 0);
			ReadData2:   out std_logic_vector(31 downto 0);
			Ext_Imm :    out std_logic_vector(31 downto 0);
			Func :       out std_logic_vector(5 downto 0);
			SA :         out std_logic_vector(4 downto 0));		
end IDecode;

architecture Behavioral of IDecode is

signal ReadAddress1: std_logic_vector(4 downto 0);
signal ReadAddress2: std_logic_vector(4 downto 0);
signal WriteAddress: std_logic_vector(4 downto 0):="00000";
signal ExtImm:       std_logic_vector(31 downto 0);

begin

--------------------SIGN EXTEND--------------------
process(ExtOp,Instruction)   
begin
	case (ExtOp) is
		when '1' => 	
				case (Instruction(15)) is
					when '0' => ExtImm <= X"0000" & Instruction(15 downto 0);
					when '1' =>  ExtImm <=	X"1111" & Instruction(15 downto 0);
					when others => ExtImm <= ExtImm;
				end case;
		when others => ExtImm <= X"0000" & Instruction(15 downto 0);
	end case;
end process;
---------------------------------------------------

--------------------MUX SELECTIE RT/RD--------------------
process(RegDst,Instruction)	
begin
	case (RegDst) is
		when '0'      => WriteAddress <= Instruction(20 downto 16);    --ajunge campul rt
		when '1'      => WriteAddress <= Instruction(15 downto 11);    --ajunge campul rd
		when others   => WriteAddress <= WriteAddress;
	end case;
end process;
---------------------------------------------------
	
Func    <=Instruction(5 downto 0);	     -----FUNC - PENTRU ALU CONTROL-----
SA      <=Instruction(10 downto 6);		-----SHIFT AMOUNT-----
Ext_Imm <= ExtImm;					-----SIGN EXTEND OUT-----

ReadAddress1<=Instruction(25 downto 21);		-----READ ADDRESS 1 - RS-----
ReadAddress2<=Instruction(20 downto 16);		-----READ ADDRESS 2 - RT-----


------Instantiere componenta REG_FILE--------------
--RF1: reg_file port map (clk,ReadAddress1,ReadAddress2,WriteAddress,WriteData,RegWrite,RegWrite2,ReadData1,ReadData2);
-------------------------------------------------
RF1: entity work.reg_file Port map(
            	clk          => clk,
				ReadAddress1 => ReadAddress1,
				ReadAddress2 => ReadAddress2,
				WriteAddress => WriteAddress,
				WriteData    => WriteData,
				RegWrite     => RegWrite,
				EnableMPG    => RegWrite2,
				ReadData1    => ReadData1,
				ReadData2    => ReadData2);

end Behavioral;

