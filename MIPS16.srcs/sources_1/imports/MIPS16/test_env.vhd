----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:21:50 03/03/2015 
-- Design Name: 
-- Module Name:    test_env - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test_env is
    Port ( 
           clk: in  STD_LOGIC;
           btn: in  STD_LOGIC_VECTOR (3 downto 0);
           sw:  in  STD_LOGIC_VECTOR (7 downto 0);
           led: out  STD_LOGIC_VECTOR (7 downto 0);
           an:  out  STD_LOGIC_VECTOR (3 downto 0);
           cat: out  STD_LOGIC_VECTOR (6 downto 0);
           dp:  out  STD_LOGIC);
end test_env;

architecture Behavioral of test_env is

----------CU Signals---------
signal RegDst:  std_logic;
signal ExtOp:   std_logic;
signal ALUSrc:  std_logic;
signal Branch:  std_logic;
signal Jump:    std_logic;
signal ALUOp:   std_logic_vector(2 downto 0);
signal MemWrite: std_logic;
signal MemtoReg: std_logic;
signal RegWrite: std_logic;
-----------------------------


signal enable:      std_logic;                                --Instruction Fetch - enable
signal enable2:     std_logic;	                              --Instruction Fetch - reset
signal BranchAddress:std_logic_vector(31 downto 0);  	      --branch address used for IF
signal JumpAddress: std_logic_vector(31 downto 0); 		      --jump address used for IF
signal SSDOUT :     std_logic_vector(15 downto 0):=X"0000";   --afisare SSD
signal InstrOut:    std_logic_vector(31 downto 0);			  --next instruction  
signal PCout:       std_logic_vector(31 downto 0);			  --program counter	 
signal ALURes:      std_logic_vector(31 downto 0);			  --ALU result
signal ZeroSignal:  std_logic;								  --Zero Signal used for Branch
signal RD1:         std_logic_vector(31 downto 0);				--Read Data 1 
signal RD2:         std_logic_vector(31 downto 0);				--Read Data 2
signal Ext_Imm :    std_logic_vector(31 downto 0);				--Ext_Imm
signal Func:        std_logic_vector(5 downto 0);				--Func - Instruction(5 downto 0)
signal SA:          std_logic_vector(4 downto 0);				--Shift Amount
signal MemData:     std_logic_vector(31 downto 0);				--Mem Data = Read Data --Memory -- OUT
signal ALUResFinal: std_logic_vector(31 downto 0);			    --ALU Result - OUT from Memory
signal WriteDataReg:std_logic_vector(31 downto 0);		        --WriteData for RegFile
signal PCSrc:       std_logic;									--PCSrc signal = BranchAddress and ZeroSignal

begin

dp <= '0';

--------Instantiere componenta IFetch--------------
--IFetch1: IFetch port map(enable,enable2,clk,BranchAddress,JumpAddress,Jump,PCSrc,InstrOut,PCout);
IFetch1: entity work.IFetch port map(
            WE           => enable,
			reset        => enable2,
			clk          => clk,
			BranchAddress=> BranchAddress,
			JumpAddress  => JumpAddress,
			JCS          => Jump,
			PCSrc        => PCSrc,
			Instruction  => InstrOut,
			PC           => PCout);
-------------------------------------------------

--------Instantiere componenta IDecode-------------
--IDecode1 : IDecode port map (clk,InstrOut,WriteDataReg,RegWrite,enable,RegDst,ExtOp,RD1,RD2,Ext_Imm,Func,SA);
IDecode1: entity work.IDecode port map( 
            clk          => clk,
			Instruction  => InstrOut,
			WriteData    => WriteDataReg,
			RegWrite     => RegWrite,
			RegWrite2    => enable,
			RegDst       => RegDst,
			ExtOp        => ExtOp,
			ReadData1    => RD1,
			ReadData2    => RD2,
			Ext_Imm      => Ext_Imm,
			Func         => Func,
			SA           => SA);
---------------------------------------

--------Instantiere componenta CtrlUnit------------
--CUnit1 : ControlUnit port map (InstrOut(15 downto 13),RegDst,ExtOp,ALUSrc,Branch,Jump,ALUOp,MemWrite,MemtoReg,RegWrite);
-------------Componenta ControlUnit----
CUnit1: entity work.ControlUnit Port map(
            Instr       => InstrOut(31 downto 26),
            RegDst      => RegDst,
            ExtOp       => ExtOp,
            ALUSrc      => ALUSrc,
            Branch      => Branch,
            Jump        => Jump,
            ALUOp       => ALUOp,
            MemWrite    => MemWrite,
            MemtoReg    => MemtoReg,
            RegWrite    => RegWrite);
---------------------------------------

--------Instantiere componenta ExUnit--------------
--ExUnit1 : ExecutionUnit port map(PCOut,RD1,RD2,Ext_Imm,Func,SA,ALUSrc,ALUOp,BranchAddress,ALURes,ZeroSignal);
ExUnit1: entity work.ExecutionUnit Port map(
            PCOut           => PCOut,
            RD1             => RD1,
            RD2             => RD2,
            Ext_Imm         => Ext_Imm,
            Func            => Func,
            SA              => SA,
            ALUSrc          => ALUSrc,
            ALUOp           => ALUOp,
            BranchAddress   => BranchAddress,
            ALURes          => ALURes,
            ZeroSignal      => ZeroSignal);
---------------------------------------

--------Instantiere componenta Memory--------------
--Memory1 : MEM port map(clk,ALURes,RD2,MemWrite,enable,MemData,ALUResFinal);
Memory1: entity work.MEM port map(
			clk          => clk,
			ALURes       => ALURes,
			WriteData    => RD2,
			MemWrite     => MemWrite,
			MemWriteCtrl => enable,
			MemData      => MemData,
			ALURes2      => ALUResFinal);
---------------------------------------

------Instantiere componenta SSD, s1-------------
--s1 :SSD port map(clk,SSDOUT,an,cat);
s1: entity work.SSD port map(
            clk     => clk,
            digits  => SSDOUT,
            an      => an,
            cat     => cat);
---------------------------------------

------Instantiere componenta MPG, c1-------------
--c1 :MPG port map(btn(0),clk,enable);
--c2 :MPG port map(btn(1),clk,enable2);
c1: entity work.MPG	port map(
            btn     => btn(0),
            clock   => clk,
            enable  => enable);
---------------------------------------
c2: entity work.MPG	port map(
            btn     => btn(1),
            clock   => clk,
            enable  => enable2);
---------------------------------------

------------MUX MEM_ALU -> WriteRegister---------
process(MemtoReg, ALUResFinal, MemData)
begin
	case (MemtoReg) is
		when '1'      => WriteDataReg <= MemData;
		when '0'      => WriteDataReg <= ALUResFinal;
		when others   => WriteDataReg <= WriteDataReg;
	end case;
end process;	
-------------------------------------------------

-------------------AND Zero+Branch---------------
PCSrc<=ZeroSignal and Branch;
-------------------------------------------------

-----------------JumpAddress---------------------
JumpAddress<=PCOut(31 downto 28) & InstrOut(25 downto 0) & "00";
-------------------------------------------------

---------MUX pentru afisare----------------------
process(InstrOut,PCout,RD1,RD2,Ext_Imm,ALURes,MemData,WriteDataReg,sw)
begin
	case(sw(7 downto 5)) is
		when "000" =>	SSDOut<=InstrOut(15 downto 0);			-----AFISARE INSTROUT-----
		when "001" =>	SSDOut<=PCout(15 downto 0);				-----AFISARE PCOUT-----
		when "010" =>	SSDOut<=RD1(15 downto 0);				-----AFISARE ReadData1-----
		when "011" =>	SSDOut<=RD2(15 downto 0);				-----AFISARE ReadData2-----
		when "100" =>	SSDOut<=Ext_Imm(15 downto 0);			-----AFISARE EXT_IMM-----
		when "101" =>	SSDOut<=ALURes(15 downto 0);			-----AFISARE ALUres-----		
		when "110" =>   SSDOut<=MemData(15 downto 0);			-----AFISARE MemData-----
		when "111" =>	SSDOut<=WriteDataReg(15 downto 0);	    -----AFISARE WriteData - RegisterFile-----
		when others=>	SSDOut<=X"AAAA";
	end case;
end process;
-------------------------------------------------	

---------MUX pentru afisarea pe leduri a semnalelor de control------------
process(RegDst,ExtOp,ALUSrc,Branch,Jump,MemWrite,MemtoReg,RegWrite,sw,ALUOp)
begin
	if sw(0)='0' then		
		led(7) <= RegDst;
		led(6) <= ExtOp;
		led(5) <= ALUSrc;
		led(4) <= Branch;
		led(3) <= Jump;
		led(2) <= MemWrite;
		led(1) <= MemtoReg;
		led(0) <= RegWrite;		
	else
		led(2 downto 0) <= ALUOp(2 downto 0);
		led(7 downto 3) <= "00000";
	end if;
end process;	
---------------------------------------------------------------------------

end Behavioral;

